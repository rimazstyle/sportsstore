﻿using Microsoft.AspNetCore.Builder;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace SportStore.Models
{
    public class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder application)
        {
            ApplicationDbContext context = application.ApplicationServices.GetRequiredService<ApplicationDbContext>();
            context.Database.Migrate();
            if (!context.Products.Any())
            {
                context.Products.AddRange(
                    new Product
                    {
                        Name = "Kayak",
                        Description = "A boat for one person",
                        Category = "Watersport",
                        Price = 275M
                    },
                    new Product
                    {
                        Name = "Lifejacket",
                        Description = "Protective and fashinable",
                        Category = "Watersport",
                        Price = 45.50M
                    },
                    new Product
                    {
                        Name = "Soccer Ball",
                        Description = "FIFA-approved size and weight",
                        Category = "Soccer",
                        Price = 19.45M
                    },
                    new Product
                    {
                        Name = "Corner Flags",
                        Description = "Give you playing field a professional touch",
                        Category = "Soccer",
                        Price = 34.95M
                    },
                    new Product
                    {
                        Name = "Flag",
                        Description = "Give you playing field a professional touch",
                        Category = "Soccer",
                        Price = 23.95M
                    },
                    new Product
                    {
                        Name = "Headband",
                        Description = "Protects your head from sun",
                        Category = "Fashion",
                        Price = 9.20M
                    },
                    new Product
                    {
                        Name = "Unsteady Chair",
                        Description = "Secretly give your opponent a disadvantage",
                        Category = "Chess",
                        Price = 75M
                    });
                context.SaveChanges();
            }
        }
    }
}

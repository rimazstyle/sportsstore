﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using SportStore.Models;
using SportStore.Models.ViewModels;

namespace SportStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository repository;
        public int pageSize = 4;

        public ProductController(IProductRepository repo)
        {
            repository = repo;
        }

        public ViewResult List(string category, int productPage = 1) => View(new ProductsListViewModel
        {
            Products = repository.Products
            .Where(p => p.Category == null || p.Category == category)
            .OrderBy(x => x.ProductId)
            .Skip((productPage - 1) * pageSize)
            .Take(pageSize),
            PagingInfo = new PagingInfo
            {
                CurrentPage = productPage,
                ItemsPerPage = pageSize,
                TotalItems = category == null ?
                repository.Products.Count():
                repository.Products.Where(x => x.Category == category).Count()
            },
            CurrentCategory = category
        });
    }
}
